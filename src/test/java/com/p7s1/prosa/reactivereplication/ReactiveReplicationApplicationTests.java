package com.p7s1.prosa.reactivereplication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(properties = {"replicationrunner.enabled=false"})
class ReactiveReplicationApplicationTests {

    @Test
    void contextLoads() throws InterruptedException {

    }

}
