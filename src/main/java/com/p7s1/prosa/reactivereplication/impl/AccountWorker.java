package com.p7s1.prosa.reactivereplication.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.p7s1.prosa.reactivereplication.api.*;
import com.p7s1.prosa.reactivereplication.api.ddb.DynamoDBHelper;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.services.dynamodb.model.ScalarAttributeType;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.Date;

@Component
public class AccountWorker extends Worker<AccountWorker.DDBAccount> {
    public static final Duration TABLE_ACTIVE_DURATION = Duration.ofSeconds(30);

    private final DynamoDBHelper helper;
    private final DynamoDBMapper mapper;
    private Mono<Void> ready;

    public AccountWorker(TableQuerier tableQuerier,
                         OffsetHandler offsetHandler,
                         ReplicationConfiguration replicationConfiguration,
                         DynamoDBHelper helper,
                         DynamoDBMapper mapper) {
        super(tableQuerier, offsetHandler, "__account", replicationConfiguration);
        this.helper = helper;
        this.mapper = mapper;
    }

    @Override
    public Mono<Void> becomeReady() {
        return helper.createTableAndWaitUntilActive("prosa__account", "id", ScalarAttributeType.S, TABLE_ACTIVE_DURATION);
    }

    @Override
    public RowMapper<DDBAccount> rowMapper() {
        return new AccountRowMapper();
    }

    @Override
    public Mono<DDBAccount> onCreate(DDBAccount account) {
        return Mono.fromCallable(() -> {
            mapper.save(account);
            return account;
        });
    }

    @Override
    public Mono<DDBAccount> onUpdate(DDBAccount account) {
        return Mono.fromCallable(() -> {
            mapper.save(account);
            return account;
        });
    }

    @Override
    public Mono<DDBAccount> onDelete(DDBAccount account) {
        return Mono.fromCallable(() -> {
            mapper.delete(account);
            return account;
        });
    }


    public static class AccountRowMapper implements RowMapper<DDBAccount> {

        @Override
        public DDBAccount mapRow(ResultSet rs, int rowNum) throws SQLException {
            return DDBAccount.builder()
                    .id("account:" + rs.getInt(DDBAccount.Fields.id))
                    .customer_id("customer:" + rs.getInt(DDBAccount.Fields.customer_id))
                    .agency_id("agency:" + rs.getInt(DDBAccount.Fields.agency_id))
                    .medium_id("medium:" + rs.getInt(DDBAccount.Fields.medium_id))
                    .business_year(rs.getInt(DDBAccount.Fields.business_year))
                    .budget(rs.getBigDecimal(DDBAccount.Fields.budget))
                    .low_budget_limit(rs.getBigDecimal(DDBAccount.Fields.low_budget_limit))
                    .overdraw(rs.getInt(DDBAccount.Fields.overdraw))
                    .storno_period(rs.getInt(DDBAccount.Fields.storno_period))
                    .user_name(rs.getString(DDBAccount.Fields.user_name))
                    .last_update(rs.getDate(DDBAccount.Fields.last_update))
                    .version(rs.getInt(DDBAccount.Fields.version))
                    .__index(rs.getLong(DDBAccount.Fields.__index))
                    .__operation(rs.getString(DDBAccount.Fields.__operation))
                    .build();
        }
    }


    @Data
    @DynamoDBTable(tableName = "prosa__account")
    @Builder
    @FieldNameConstants
    public static class DDBAccount implements PollItem {

        @DynamoDBHashKey(attributeName = "id")
        private String id;
        private String customer_id;
        private String agency_id;
        private String medium_id;
        private int business_year;
        private BigDecimal budget;
        private BigDecimal low_budget_limit;
        private int overdraw;
        private int storno_period;
        private String user_name;
        private Date last_update;
        private long version;
        @DynamoDBIgnore
        private String __operation;
        @DynamoDBIgnore
        private long __index;


        @Override
        @DynamoDBIgnore
        public long get__Index__() {
            return get__index();
        }

        @Override
        @DynamoDBIgnore
        public String get__Operation__() {
            return get__operation();
        }

        @Override
        @DynamoDBIgnore
        public String get__Table__() {
            return "__account";
        }
    }


}
