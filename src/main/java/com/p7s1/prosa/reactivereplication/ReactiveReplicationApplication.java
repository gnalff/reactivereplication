package com.p7s1.prosa.reactivereplication;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;

import java.time.Duration;

@SpringBootApplication
public class ReactiveReplicationApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveReplicationApplication.class, args);
    }

    @Bean
    public DynamoDbAsyncClient dynamoDB() {
        return DynamoDbAsyncClient
                .builder()
                .region(Region.EU_CENTRAL_1)
                .overrideConfiguration(builder -> {
                    builder.apiCallAttemptTimeout(Duration.ofSeconds(2))
                            .apiCallTimeout(Duration.ofSeconds(2));
                })
                .build();
    }

    @Bean
    public AmazonDynamoDB dynamoDbClient() {
        return AmazonDynamoDBClientBuilder.standard()
                .withRegion("eu-central-1")
                .withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
                .build();
    }

    @Bean
    public DynamoDBMapper dynamoDBMapper(AmazonDynamoDB dynamoDBClient) {
        DynamoDBMapperConfig mapperConfig = DynamoDBMapperConfig.builder()
                .withSaveBehavior(DynamoDBMapperConfig.SaveBehavior.CLOBBER)
                .withConsistentReads(DynamoDBMapperConfig.ConsistentReads.EVENTUAL)
                .withPaginationLoadingStrategy(DynamoDBMapperConfig.PaginationLoadingStrategy.EAGER_LOADING)
                .build();
        return new DynamoDBMapper(dynamoDBClient, mapperConfig);
    }
}
