package com.p7s1.prosa.reactivereplication.api;

import reactor.core.publisher.Mono;

public interface OffsetHandler {
    Mono<Long> getOffsetForTable(String table);

    Mono<Long> writeOffsetForTable(long offset, String table);

}
