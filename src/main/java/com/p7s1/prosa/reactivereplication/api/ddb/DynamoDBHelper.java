package com.p7s1.prosa.reactivereplication.api.ddb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.retry.Repeat;
import reactor.retry.Retry;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;

@Component
@Slf4j
public class DynamoDBHelper {

    public static final Duration DYNAMO_CLIENT_TIMEOUT = Duration.ofSeconds(1);
    public static final int MAX_FAILING_RETRIES = 5;
    public static final Duration MIN_FAILING_BACKOFF_TIME = Duration.ofSeconds(2);
    public static final Duration MAX_FAILING_BACKOFF_TIME = Duration.ofSeconds(5);
    public static final Duration MIN_REPEAT_BACKOFF = Duration.ofSeconds(1);
    public static final Duration MAX_REPEAT_BACKOFF = Duration.ofSeconds(5);
    public static final Duration DESCRIBE_AFTER_CREATE_DELAY = Duration.ofSeconds(2);
    private final DynamoDbAsyncClient dynamoDbAsyncClient;

    public DynamoDBHelper(DynamoDbAsyncClient dynamoDbAsyncClient) {
        this.dynamoDbAsyncClient = dynamoDbAsyncClient;
    }


    /**
     * Mono for checking if a table already exists
     * times out after 1s
     *
     * @param name name of the table
     * @return true-mono if table exists otherwise false mono
     */
    private Mono<Boolean> existsTable(String name) {
        return Mono
                .fromFuture(
                        sendListTableRequest()
                )
                .timeout(DYNAMO_CLIENT_TIMEOUT)
                .map(listTablesResponse -> listTablesResponse.tableNames())
                .map(strings -> strings.contains(name))
                .doOnNext(exists -> log.debug("Table {} exists: {}", name, exists));
    }

    private CompletableFuture<ListTablesResponse> sendListTableRequest() {
        log.info("sendListTableRequest");
        return dynamoDbAsyncClient.listTables();
    }

    private Mono<Void> waitForTableActive(String name, Duration timeout) {
        return isTableReady(name)
                .flatMap(ready -> {
                    if (ready) {
                        return Mono.just(true);
                    }
                    return Mono.empty();
                })
                .retryWhen(Retry
                        .allBut(ResourceNotFoundException.class)
                        .retryMax(MAX_FAILING_RETRIES)
                        .exponentialBackoffWithJitter(MIN_FAILING_BACKOFF_TIME, MAX_FAILING_BACKOFF_TIME)
                        .doOnRetry(objectRetryContext -> {
                            log.warn("Retrying waitForTableCreated {}", name, objectRetryContext.exception());
                        })
                )
                .repeatWhenEmpty(Repeat
                        .onlyIf(repeatContext -> true)
                        .timeout(timeout)
                        .exponentialBackoffWithJitter(MIN_REPEAT_BACKOFF, MAX_REPEAT_BACKOFF)
                        .doOnRepeat(objectRepeatContext -> {
                            log.debug("Repeating waitForTableCreated {}", name);
                        })
                )
                .switchIfEmpty(Mono.error(new TimeoutException("Table " + name + " did not become active")))
                .then();
    }

    private Mono<Boolean> isTableReady(String name) {
        return tableStatus(name)
                .map(tableStatus -> tableStatus == TableStatus.ACTIVE);
    }

    private Mono<TableStatus> tableStatus(String name) {
        return describeTable(name)
                .map(describeTableResponse -> describeTableResponse.table().tableStatus())
                .doOnNext(tableStatus -> log.debug("tableStatus {} - {}", name, tableStatus));
    }

    private Mono<DescribeTableResponse> describeTable(String name) {
        return Mono.fromFuture(sendDescribeTableRequest(name)).timeout(DYNAMO_CLIENT_TIMEOUT);
    }

    private CompletableFuture<DescribeTableResponse> sendDescribeTableRequest(String name) {
        log.debug("sendDescribeTableRequest {}", name);
        return dynamoDbAsyncClient.describeTable(DescribeTableRequest
                .builder()
                .tableName(name)
                .build());
    }


    public Mono<Void> createTableAndWaitUntilActive(String tableName, String partitionKeyName, ScalarAttributeType partitionKeyType, Duration untilActive) {
        return Mono
                .defer(() -> createTableIfNotExists(tableName, partitionKeyName, partitionKeyType)
                        .then(Mono.delay(DESCRIBE_AFTER_CREATE_DELAY)
                                .then(
                                        waitForTableActive(tableName, untilActive)
                                )
                        ))
                .retryWhen(
                        Retry.any()
                                .retryMax(MAX_FAILING_RETRIES)
                                .exponentialBackoffWithJitter(MIN_FAILING_BACKOFF_TIME, MAX_FAILING_BACKOFF_TIME)
                                .doOnRetry(objectRetryContext -> log.warn("retrying createTableAndWaitUntilActive {}", tableName, objectRetryContext.exception()))
                );
    }

    private Mono<Boolean> createTableIfNotExists(String tableName, String partitionKeyName, ScalarAttributeType partitionKeyType) {
        return existsTable(tableName)
                .flatMap(exists -> {
                    if (exists) {
                        return Mono.just(false);
                    }
                    return createTable(tableName, partitionKeyName, partitionKeyType)
                            .map(createTableResponse -> true)
                            .onErrorResume(throwable -> throwable instanceof ResourceInUseException, throwable -> {
                                log.info("Resuming createTable from {} by continuing", throwable.getClass().getSimpleName());
                                return Mono.just(false);
                            }).doOnSuccess(created -> {
                                log.debug("created new table {}", tableName);
                            });
                });
    }

    private Mono<CreateTableResponse> createTable(String tableName, String partitionKeyName, ScalarAttributeType partitionKeyType) {
        return Mono.fromFuture(sendCreateTableRequest(tableName, partitionKeyName, partitionKeyType)).timeout(DYNAMO_CLIENT_TIMEOUT)
                .doOnNext(createTableResponse -> {
                    log.info("createTable {} - {}", createTableResponse.tableDescription().tableName(), createTableResponse.tableDescription().tableStatus());
                });
    }

    private CompletableFuture<CreateTableResponse> sendCreateTableRequest(String tableName, String partitionKeyName, ScalarAttributeType partitionKeyType) {
        return dynamoDbAsyncClient.createTable(CreateTableRequest.builder()
                .tableName(tableName)
                .keySchema(
                        KeySchemaElement.builder().attributeName(partitionKeyName).keyType(KeyType.HASH).build()
                )
                .sseSpecification(
                        SSESpecification.builder().enabled(true).build()
                )
                .billingMode(BillingMode.PAY_PER_REQUEST)
                .attributeDefinitions(builder -> builder.attributeName(partitionKeyName).attributeType(partitionKeyType))
                .build());
    }

}
