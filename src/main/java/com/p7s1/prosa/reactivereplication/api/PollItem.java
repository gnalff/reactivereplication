package com.p7s1.prosa.reactivereplication.api;

public interface PollItem {
    long get__Index__();

    String get__Operation__();

    String get__Table__();
}
