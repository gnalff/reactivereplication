package com.p7s1.prosa.reactivereplication.api;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.convert.DurationUnit;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Configuration
@ConfigurationProperties(prefix = "replication")
@Data
public class ReplicationConfiguration {

    private Map<String, TablePollConfiguration> tables = new HashMap<>();

    @Data
    public static class TablePollConfiguration {

        @DurationUnit(ChronoUnit.SECONDS)
        private Duration pollTimeout = Duration.ofSeconds(1);
        private int maxRetries = 10;
        @DurationUnit(ChronoUnit.SECONDS)
        private Duration retryMinBackOff = Duration.ofSeconds(10);
        @DurationUnit(ChronoUnit.SECONDS)
        private Duration retryMaxBackOff = Duration.ofSeconds(10);
        @DurationUnit(ChronoUnit.SECONDS)
        private Duration minInterval = Duration.ofSeconds(5);
        private Duration maxInterval = Duration.ofSeconds(5);
        private int batchSize = 10;

    }

    public Optional<TablePollConfiguration> forTable(String table) {
        return Optional.ofNullable(tables.get(table));
    }
}
