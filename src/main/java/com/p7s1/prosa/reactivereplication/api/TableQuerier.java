package com.p7s1.prosa.reactivereplication.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.retry.Repeat;

import java.util.List;

@Component
@Slf4j
public class TableQuerier {

    private final JdbcTemplate jdbcTemplate;

    public TableQuerier(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public <T extends PollItem> Mono<List<T>> poll(String table, long lastFetched, RowMapper<T> rowMapper, ReplicationConfiguration.TablePollConfiguration configuration) {
        return Mono.
                fromCallable(() -> {
                    log.debug("Querying table: {} with index: {}", table, lastFetched);
                    return jdbcTemplate.query(
                            "Select * from " + table + " where __index > " + lastFetched + " order by __index asc limit " + configuration.getBatchSize() + ";", rowMapper);
                })
                .timeout(configuration.getPollTimeout());
    }

    public <T extends PollItem> Flux<T> pollUntilNotEmpty(String table, long lastFetched, RowMapper<T> rowMapper, ReplicationConfiguration.TablePollConfiguration configuration) {
        return poll(table, lastFetched, rowMapper, configuration)
                .flatMap(list -> {
                    if (list.isEmpty()) {
                        return Mono.empty();
                    }
                    return Mono.just(list);
                })
                .repeatWhenEmpty(Repeat
                        .times(Long.MAX_VALUE)
                        .fixedBackoff(configuration.getPollTimeout())
                        .doOnRepeat(repeatContext -> {
                            log.debug("Nothing fetched repolling {} at {}", table, lastFetched);
                        })
                )
                .flatMapMany(Flux::fromIterable);
    }


}
