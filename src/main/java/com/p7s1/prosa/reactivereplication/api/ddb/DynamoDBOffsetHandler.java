package com.p7s1.prosa.reactivereplication.api.ddb;

import com.p7s1.prosa.reactivereplication.api.OffsetHandler;
import lombok.Data;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.retry.Retry;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;
import software.amazon.awssdk.services.dynamodb.model.*;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Long.valueOf;
import static reactor.core.publisher.Mono.fromFuture;

@Slf4j
@Component
public class DynamoDBOffsetHandler implements OffsetHandler {

    public static final Duration UNTIL_ACTIVE = Duration.ofSeconds(30);
    public static final Duration DYNAMO_OFFSET_HANDLER_TIMEOUT = Duration.ofSeconds(1);
    public static final Duration MIN_BACKOFF_OFFSET_CREATE = Duration.ofSeconds(60);
    public static final Duration MAX_BACKOFF_OFFSET_CREATE = Duration.ofSeconds(120);
    private final DynamoDBOffsetConfiguration configuration;
    private final DynamoDbAsyncClient dynamoDbAsyncClient;
    private final DynamoDBHelper dynamoDBHelper;
    private Mono<Void> ready;

    private ConcurrentHashMap<String, Long> offsets = new ConcurrentHashMap();

    public DynamoDBOffsetHandler(DynamoDBOffsetConfiguration configuration, DynamoDbAsyncClient dynamoDbAsyncClient, DynamoDBHelper dynamoDBHelper) {
        this.configuration = configuration;
        this.dynamoDbAsyncClient = dynamoDbAsyncClient;
        this.dynamoDBHelper = dynamoDBHelper;
    }

    @PostConstruct
    public void init() {
        this.ready = becomeReady().cache();
    }

    private Mono<Void> becomeReady() {
        return dynamoDBHelper
                .createTableAndWaitUntilActive(configuration.getTableName(), TableOffset.Fields.table, ScalarAttributeType.S, UNTIL_ACTIVE)
                .retryWhen(Retry
                        .any()
                        .exponentialBackoffWithJitter(MIN_BACKOFF_OFFSET_CREATE, MAX_BACKOFF_OFFSET_CREATE)
                        .retryMax(5)
                        .doOnRetry(retryContext -> {
                            log.warn("retrying createTableAndWaitUntilActive for offset table", retryContext.exception());
                        })
                );
    }

    @Override
    public Mono<Long> getOffsetForTable(String table) {
        return ready
                .then(
                        Mono.defer(() -> Mono
                                .fromSupplier(() -> offsets.getOrDefault(table, null))
                                .switchIfEmpty(Mono.defer(() -> offset(table)
                                )))
                );
    }


    @Override
    public Mono<Long> writeOffsetForTable(long offset, String table) {
        return ready
                .then(
                        Mono.defer(() -> {
                            return Mono
                                    .fromFuture(sendPutItemRequest(offset, table))
                                    .timeout(DYNAMO_OFFSET_HANDLER_TIMEOUT)
                                    .flatMap(putItemResponse -> Mono.just(offset))
                                    .doOnNext(o -> {
                                        log.debug("memorizing offset {} for table", offset, table);
                                        offsets.put(table, o);
                                    });
                        }));
    }

    private Mono<Long> offset(String table) {
        return fromFuture(sendGetItemRequest(table))
                .timeout(DYNAMO_OFFSET_HANDLER_TIMEOUT)
                .map(getItemResponse -> {
                    if (getItemResponse.hasItem()) {
                        return valueOf(getItemResponse.item().get(TableOffset.Fields.offset).n());
                    } else {
                        return 0L;
                    }
                })
                .doOnNext(offset -> {
                    log.debug("resolved offset {} for table {}", offset, table);
                });
    }

    private CompletableFuture<GetItemResponse> sendGetItemRequest(String table) {
        log.debug("getting offset from table {}", table);
        return dynamoDbAsyncClient.getItem(GetItemRequest.builder()
                .attributesToGet(TableOffset.Fields.offset)
                .consistentRead(false)
                .tableName(configuration.getTableName())
                .key(
                        Map.of(
                                TableOffset.Fields.table, AttributeValue.builder().s(table).build()))
                .build());
    }

    private CompletableFuture<PutItemResponse> sendPutItemRequest(long offset, String table) {
        log.debug("writing offset to table {}", table);
        return dynamoDbAsyncClient.putItem(
                PutItemRequest
                        .builder()
                        .tableName(configuration.getTableName())
                        .item(Map.of(
                                TableOffset.Fields.table, AttributeValue.builder().s(table).build(),
                                TableOffset.Fields.offset, AttributeValue.builder().n(String.valueOf(offset)).build()
                        ))
                        .build()
        );
    }

    @ConfigurationProperties(prefix = "dynamodboffset")
    @Configuration
    @Data
    public static class DynamoDBOffsetConfiguration {
        private String tableName;
    }

    @Data
    @FieldNameConstants
    public static class TableOffset {
        private String table;
        private long offset;
    }


}
