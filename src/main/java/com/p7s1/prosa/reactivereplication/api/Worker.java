package com.p7s1.prosa.reactivereplication.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.retry.Repeat;
import reactor.retry.Retry;

import java.time.Duration;


@Slf4j
public abstract class Worker<T extends PollItem> {

    public static final Duration MAX_WORKER_CYCLE_TIME = Duration.ofSeconds(2);
    private final TableQuerier tableQuerier;
    private final OffsetHandler offsetHandler;
    private String sourceTableName;
    private final ReplicationConfiguration.TablePollConfiguration configuration;

    public Worker(TableQuerier tableQuerier,
                  OffsetHandler offsetHandler,
                  String sourceTableName,
                  ReplicationConfiguration replicationConfiguration) {
        this.tableQuerier = tableQuerier;
        this.offsetHandler = offsetHandler;
        this.sourceTableName = sourceTableName;
        this.configuration = replicationConfiguration.forTable(sourceTableName).get();
    }

    public Flux<T> replicate() {
        log.info("Replication started for worker on table '{}'", sourceTableName);
        return Flux
                .defer(() -> {
                    return offsetHandler.getOffsetForTable(sourceTableName)
                            .flatMapMany(offset ->
                                    tableQuerier.pollUntilNotEmpty(sourceTableName, offset, rowMapper(), configuration)
                            )
                            .concatMap(this::resolveOperationalMono)
                            .concatMap(v -> offsetHandler.writeOffsetForTable(v.get__Index__(), sourceTableName).then(Mono.just(v)));
                })
                .retryWhen(
                        Retry
                                .any()
                                .randomBackoff(configuration.getRetryMinBackOff(), configuration.getRetryMaxBackOff())
                                .retryMax(configuration.getMaxRetries())
                                .doOnRetry(retryContext -> {
                                    log.warn("retrying to poll {} with offset {}", sourceTableName, retryContext.exception());
                                })
                )
                .repeatWhen(
                        Repeat
                                .onlyIf(repeatContext -> true)
                                .randomBackoff(configuration.getMinInterval(), configuration.getMaxInterval())
                );
    }

    protected Mono<T> resolveOperationalMono(T t) {
        log.debug("Received from table {}: {}", sourceTableName, t);
        if ("insert".equals(t.get__Operation__())) {
            log.debug("will call onCreate for {} because operation is {}", t.get__Table__(), t.get__Operation__());
            return onCreate(t)
                    .timeout(MAX_WORKER_CYCLE_TIME)
                    .doOnNext(onCreate -> {
                        log.debug("onCreate emitted index {} for {}-{}", onCreate.get__Index__(), onCreate.get__Table__(), onCreate.get__Operation__());
                    });
        } else if ("update".equals(t.get__Operation__())) {
            log.debug("will call onUpdate for {} because operation is {}", t.get__Table__(), t.get__Operation__());
            return onUpdate(t)
                    .timeout(MAX_WORKER_CYCLE_TIME)
                    .doOnNext(onUpdate -> {
                        log.debug("onUpdate emitted index {} for {}-{}", onUpdate.get__Index__(), onUpdate.get__Table__(), onUpdate.get__Operation__());
                    });
        } else if ("delete".equals(t.get__Operation__())) {
            log.debug("will call onDelete for {} because operation is {}", t.get__Table__(), t.get__Operation__());
            return onDelete(t)
                    .timeout(MAX_WORKER_CYCLE_TIME)
                    .doOnNext(onDelete -> {
                        log.debug("onDelete emitted index {} for {}-{}", onDelete.get__Index__(), onDelete.get__Table__(), onDelete.get__Operation__());
                    });
        } else {
            log.warn("unknown operation for {} because operation is >{}<", t.get__Table__(), t.get__Operation__());
            return Mono.error(new IllegalArgumentException("unknown operation provided >" + t.get__Operation__() + "<"));
        }
    }

    public abstract RowMapper<T> rowMapper();

    public abstract Mono<T> onCreate(T t);

    public abstract Mono<T> onUpdate(T t);

    public abstract Mono<T> onDelete(T t);

    public abstract Mono<Void> becomeReady();


}
