package com.p7s1.prosa.reactivereplication;

import com.p7s1.prosa.reactivereplication.api.Worker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import reactor.retry.Retry;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@Slf4j
@Component
@ConditionalOnProperty(prefix = "replicationrunner", name = "enabled", havingValue = "true", matchIfMissing = true)
public class ReplicationRunner implements ApplicationRunner {

    private final ApplicationContext applicationContext;

    public ReplicationRunner(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Map<String, Worker> foundWorkers = applicationContext.getBeansOfType(Worker.class);
        log.info("Workers aggregated: {}", foundWorkers.size());

        CountDownLatch latch = new CountDownLatch(foundWorkers.size());

        foundWorkers
                .values()
                .stream()
                .forEach(w -> {
                    startWorker(latch, w);
                });
        latch.await();
    }

    private void startWorker(CountDownLatch latch, Worker<?> worker) {
        String workerName = worker.getClass().getSimpleName();
        log.info("subscribing to worker: {}", workerName);
        worker
                .becomeReady()
                .retryWhen(Retry
                        .any()
                        .exponentialBackoffWithJitter(Duration.ofSeconds(60), Duration.ofSeconds(120))
                        .retryMax(5)
                        .doOnRetry(retryContext -> {
                            log.warn("retrying to get worker {} ready", workerName, retryContext.exception());
                        })
                )
                .doOnNext(aBoolean -> {
                    log.info("Worker >{}< became ready", workerName);
                })
                .thenMany(Flux.defer(() -> worker.replicate()))
                .subscribeOn(Schedulers.boundedElastic())
                .subscribe(pollItem -> {
                            log.debug("replicated '{}'-'{}':'{}'", new Object[]{pollItem.get__Table__(), pollItem.get__Operation__(), pollItem.get__Index__()});
                        }, throwable -> {
                            latch.countDown();
                            log.error("replication for {} stopped with error", workerName, throwable);
                        }, () -> {
                            latch.countDown();
                            log.error("replication commpleted for {}", workerName);
                        }
                );
    }
}
