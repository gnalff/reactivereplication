#Setup

1. Have an AWS Account 
2. Have your AWS configured properly so that you have a configured ~/.aws/credentials files.
See: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html
3. Pull informix to your local machine:
    1.  ``` docker pull ibmcom/informix-developer-database```
    2. follow the instructions here: https://github.com/informix/informix-dockerhub-readme/blob/master/14.10.FC1/informix-developer-database.md (Basically just run ``` docker run -it --name ifx -h ifx --privileged -p 9088:9088 -p 9089:9089 -p 27017:27017 -p 27018:27018 -p 27883:27883 -e LICENSE=accept ibmcom/informix-developer-database:latest``` )
    3. start your informix docker container ``` docker start ifx ```
    4. connect a database client 
        1. ``` jdbc:informix-sqli://localhost:9088/sysadmin:INFORMIXSERVER=informix ```
        2. ``` user: informix ```
        3. ``` password: in4mix ```
    5. create a database and an initial table
        1. ``` create database prosa ```
        2.  
        ``` 
            CREATE TABLE prosa:informix.account (
            id SERIAL NOT NULL,
            medium_id INTEGER NOT NULL,
            customer_id INTEGER NOT NULL,
            agency_id INTEGER,
            business_year INTEGER NOT NULL,	
            budget DECIMAL(15,2) NOT NULL,
            low_budget_limit DECIMAL(15,2) NOT NULL,
            overdraw INTEGER NOT NULL,
            storno_period INTEGER NOT NULL,
            user_name VARCHAR(40),
            last_update DATETIME YEAR TO FRACTION,
            version INTEGER NOT NULL
        );
        CREATE UNIQUE INDEX i_account ON prosa:informix.account (id);
        CREATE UNIQUE INDEX i_account_1 ON prosa:informix.account (medium_id,customer_id,agency_id,business_year);
        ```
4. start the replication by starting the main method in ReactiveReplicationApplication
    1. or run ``` mvn spring-boot:run ``
        
5. No tests running yet

#What happens

1. on application startup DynamoDbOffsetHandler is initialized
    1. it creates a dynamodb table with name offsets (configurable)
2. it executes the schema.sql and the data-sqls (account.sql see yml file)    
2. Replication Runner is invoked
    3. replicationrunner resolves all beans of type worker (in this case accountworker)
    4. for each worker it ensures that the worker can initialize first (become ready)
    5. then replication is started
        1. getOffset
        2. pollUntilItem
        3. create/update/delete
        3. writeOffset
        4. repeat                                                                                                                                                                  

#What to do when i want to replicate another table
1. Create a table in informix you want to replicate
2. create a sql file which creates the shadow table by executing a procedure (see account.sql)
3. Write a worker (extending Worker)
4. Try out by inserting/updating/removing data from your new table  


#What?

So basically it works like this:

1) a trigger is created which captures all insert/updates/deletes and writes it to a shadow table
which has the same columns + __index + __operation + __login + __timestamp
when u execute 'createAuditable('XXX')'
2) this shadow table is polled by a defined interval
3) when there are items in a poll cycle the worker writes them where ever to
4) the offset is stored in dynamodb table (because a dynamodboffsetwriter is used)
5) repeat


#principles

Everything should work kind of reactive thus this project is based on spring and reactor
